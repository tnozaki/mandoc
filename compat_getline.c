#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_GETLINE

int dummy;

#else

/*-
 * Copyright (c) 2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

ssize_t
getline(char **lineptr, size_t *sizptr, FILE *fp)
{
	char *line;
	ssize_t len;
	int ch;

	if (lineptr == NULL || sizptr == NULL) {
		errno = EINVAL;
		return -1;
	}
	if (*lineptr == NULL)
		*sizptr = 0;

	line = NULL;
	len = 0;

	flockfile(fp);

	do {
		if ((size_t)len >= *sizptr) {
			if (*sizptr >= SSIZE_MAX) {
				errno = EOVERFLOW;
				len = 0;
				break;
			}
			if (SSIZE_MAX - BUFSIZ < *sizptr) {
				*sizptr = SSIZE_MAX;
			} else {
				*sizptr += BUFSIZ;
			}
			line = realloc(*lineptr, *sizptr);
			if (line == NULL) {
				len = 0;
				break;
			}
			*lineptr = line;
		} else {
			line = *lineptr;
		}
		ch = getc_unlocked(fp);
		if (ch == EOF)
			 break;
		line[len++] = ch;

	} while (ch != '\n');

	funlockfile(fp);

	if (line != NULL)
		line[len] = '\0';
	if (len == 0)
		len = -1;

	return len;
}

#endif
