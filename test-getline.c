#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
	char *cp = NULL;
	ssize_t len;
        size_t sz;
        fclose(stdin);
	len = getline(&cp, &sz, stdin);
	free(cp);
	return(-1 != len);
}
